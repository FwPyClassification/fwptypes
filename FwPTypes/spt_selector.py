﻿import pyximport; pyximport.install()
import json, math, os, re
from collections import defaultdict
from operator import itemgetter
from functools import reduce
import pandas as pd
from FwText import split_strip, get_tokens_between, word_tokenize, get_phrase_position
from FwText import pluralize, singularize, clean_text
from FwFreq import freq_dict_to_percent
from FwDictionary import FwDictionary
import FwDbDict
from .taxonomy import get_taxonomy_counts, get_tree_taxonomy_counts, taxonomy_similarity

__all__ = [
    "KnownPTypeBases", "SptSelector",
    "convert_tree_node_count_to_percent", "taxonomy_probabilities",
    "get_node_importance", "get_taxonomy_confidence",
    "e_MIN_PT_DIFF", ]

a_MISSING_SCORE = [round(1 - ((1 / i) - 0.1), 4) for i in range(1, 11)]
a_MISMATCH_SCORE = [1 - round(2 - i ** 0.3, 4) for i in range(1, 11)]
a_MISSING_DELTA_SCORE = [round(y - x, 4) for x, y in zip(a_MISSING_SCORE, a_MISSING_SCORE[1:])]
a_MISMATCH_DELTA_SCORE =  [round(y - x, 4) for x, y in zip(a_MISMATCH_SCORE, a_MISMATCH_SCORE[1:])]

a_PT_LEN_SCORE = [math.log10(10-i) ** 4 for i in reversed(range(4))]
a_PT_CHAR_LEN_SCORE = [math.log10(8-i) ** 4 for i in reversed(range(7))]

c_MISSING = {
    '', 'n/a', 'na', 'none', 'missing', 'empty', 'unknown',
    ('',), ('n/a',), ('na',), ('none',), ('missing',), ('empty',), ('unknown',)}

CONJUNCTIONS = FwDictionary("fw_conjunction_words").vocabulary
PREPOSITIONS = FwDictionary("fw_preposition_words").vocabulary
DETERMINERS = FwDictionary("fw_determiner_words").vocabulary
ADJECTIVES = FwDictionary("fw_adjective_words").vocabulary
PTYPE_INDICATORS = set(
    list(FwDictionary("fw_accessory_words").vocabulary) +
    list(FwDictionary("fw_quantity_words").vocabulary)
)
c_UNIMPORTANT_PTYPE_WORDS = set(
    list(CONJUNCTIONS) + list(PREPOSITIONS) + list(DETERMINERS)
)

e_MIN_PT_DIFF = {  # how close 2nd pt has to be in order to merge instead of just selecting
    'same base, merge modifiers': 0.5,
    'same modifier, merge bases': 0.3,
    'pt1 inside pt2': 0.0,  # no merging necessary since one is contained within another
    'pt2 inside pt1': 0.0,  # no merging necessary since one is contained within another
    'merge adjacent - pt1 first': 0.4,
    'merge adjacent - pt2 first': 0.4,
    'merge using "with"': 0.25,
    'merge using "w"': 0.1,
    'merge using "and"': 0.25,
    'merge using "&"': 0.1,
    'merge pt2 words found with pt1': 0.2,
    'merge pt1 words found with pt2': 0.2,
}

def _data_full_path(path_inside_data):
    '''
    @description: concatenates the path of FwDictionary with /data/
    and the path of a vocabulary file.
    @arg path_inside_data: {str}
    @return: {str}
    '''
    return os.path.join(os.path.dirname(__file__), "data", path_inside_data)
    

class KnownPTypeBases():
    ''' Get the known bases and modifiers from a sqlite file
        and make them available as dictionary, sets, and
        a function to split new spts '''
    def __init__(self, s_db_path=''):

        self.bases, self.base_ids = {}, {}
        self.modifiers, self.modifier_ids = {}, {}
            
    def _load_db(self):
        e_bases = FwDbDict.get_sqlite_dict(
                s_path=_data_full_path("spt_gpcs.sqlite"),
                s_table="bases",
                s_keys="base",
                a_values=["modifiers"],
            )

        self.base_ids = FwDbDict.get_sqlite_dict(
                s_path=_data_full_path("spt_gpcs.sqlite"),
                s_table="bases",
                s_keys="base_id",
                a_values=["base"],
            )
        
        e_modifiers = FwDbDict.get_sqlite_dict(
                s_path=_data_full_path("spt_gpcs.sqlite"),
                s_table="modifiers",
                s_keys="modifier",
                a_values=["bases"],
            )

        self.modifier_ids = FwDbDict.get_sqlite_dict(
                s_path=_data_full_path("spt_gpcs.sqlite"),
                s_table="modifiers",
                s_keys="mod_id",
                a_values=["modifier"],
            )
        
        self.bases = {
            tuple(s_base.split()): json.loads(json_modifiers)
            for s_base, json_modifiers in e_bases.items()}
        
        self.modifiers = {
            tuple(s_mod.split()): json.loads(json_bases)
            for s_mod, json_bases in e_modifiers.items()}
        
        
    def split_base(self, pt):
        ''' Split the pt into enhancements and base. '''
        if isinstance(pt, str):
            pt = tuple(word_tokenize(pt.lower(), stop_words=False, convert_ampersand=True))
        elif isinstance(pt, list):
            pt = tuple(pt)
        
        i_base_len = len(pt)
        while i_base_len > 1:
            if pt[-i_base_len:] in self.bases:
                break
            i_base_len -= 1
        
        return (pt[:-i_base_len], pt[-i_base_len:])

    def merge_pts(self, a_title, s_pt1, s_pt2, d_score_diff=0.0):
        t_pt1 = tuple(word_tokenize(s_pt1.lower(), stop_words=False))
        t_pt2 = tuple(word_tokenize(s_pt2.lower(), stop_words=False))
        
        t_pt1_spans = get_phrase_position(a_title, t_pt1)
        t_pt2_spans = get_phrase_position(a_title, t_pt2)
        
        t_pt1_modifier, t_pt1_base  = self.split_base(t_pt1)
        t_pt2_modifier, t_pt2_base = self.split_base(t_pt2)
        
        b_spans = False if not t_pt1_spans or not t_pt2_spans else True
        if not b_spans or t_pt2_spans <= t_pt1_spans:
            b_pt2_first = True
        else:
            b_pt2_first = False
        
        b_pt1_indicated = t_pt1[-1] in PTYPE_INDICATORS
        b_pt2_indicated = t_pt2[-1] in PTYPE_INDICATORS
        
        if (
            (b_spans and t_pt1_spans[0] >= t_pt2_spans[0] and t_pt1_spans[1] <= t_pt2_spans[1]) or
            all(word in t_pt2 for word in t_pt1)
        ):
            # pt1 contained inside of pt2
            s_reason = 'pt1 inside pt2'
            t_pt = t_pt2
            
        elif (
            (b_spans and t_pt2_spans[0] >= t_pt1_spans[0] and t_pt2_spans[1] <= t_pt1_spans[1]) or
            all(word in t_pt2 for word in t_pt1)
        ):
            # pt2 contained inside of pt1
            s_reason = 'pt2 inside pt1'
            t_pt = t_pt1
            
        elif t_pt1_base and t_pt1_base == t_pt2_base:
            # merge modifiers for same base
            s_reason = 'same base, merge modifiers'
            if d_score_diff > e_MIN_PT_DIFF[s_reason]:
                t_pt, s_reason = t_pt1, 'same base, no merge'
            else:
                t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                   b_pt1_is_base=None, s_merge_type='modifiers', s_between='&',)
        
        elif t_pt1_modifier and t_pt1_modifier == t_pt2_modifier:
            # merge bases for same modifer
            s_reason = 'same modifier, merge bases'
            if d_score_diff > e_MIN_PT_DIFF[s_reason]:
                t_pt, s_reason = t_pt1, 'same modifier, no merge'
            else:
                t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                   b_pt1_is_base=None, s_merge_type='bases', s_between='&',)
                #t_pt = t_pt1_modifier + t_pt1_base + ('&',) + t_pt2_base
        
        elif b_spans and t_pt1_spans[1] == t_pt2_spans[0]:
            # adajacent - pt1 first
            s_reason = 'merge adjacent - pt1 first'
            if d_score_diff > e_MIN_PT_DIFF[s_reason]:
                t_pt, s_reason = t_pt1, 'adjacent, no merge'
            else:
                t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                   b_pt1_is_base=False, s_merge_type='', s_between='',)
            
        elif b_spans and t_pt2_spans[1] == t_pt1_spans[0]:
            # adajacent - pt2 first
            s_reason = 'merge adjacent - pt2 first'
            if d_score_diff > e_MIN_PT_DIFF[s_reason]:
                t_pt, s_reason = t_pt1, 'adjacent, no merge'
            else:
                t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                   b_pt1_is_base=True, s_merge_type='', s_between='',)
            
        else:
            if b_spans:
                a_between = get_tokens_between(a_title, t_pt1_spans, t_pt2_spans)
            else:
                a_between = []
            if len(a_between) > 4 or 'for' in a_between: a_between = []
            
            if 'for' in a_between:
                t_pt = t_pt1
                s_reason = 'no merge "for"'
                
            elif 'with' in a_between and d_score_diff <= e_MIN_PT_DIFF['merge using "with"']:
                t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                   b_pt1_is_base=None, s_merge_type='', s_between='with',)
                s_reason = 'merge using "with"'
            elif 'w' in a_between and d_score_diff <= e_MIN_PT_DIFF['merge using "w"']:
                t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                   b_pt1_is_base=None, s_merge_type='', s_between='w',)
                s_reason = 'merge using "w"'
            elif 'and' in a_between and d_score_diff <= e_MIN_PT_DIFF['merge using "and"']:
                t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                   b_pt1_is_base=None, s_merge_type='', s_between='and',)
                s_reason = 'merge using "and"'
            elif '&' in a_between and d_score_diff <= e_MIN_PT_DIFF['merge using "&"']:
                t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                   b_pt1_is_base=None, s_merge_type='', s_between='&',)
                s_reason = 'merge using "&"'
            else:
                e_pt1_modifiers = {
                    self.modifiers[self.modifier_ids[i_mod]]: i_count
                    for i_mod, i_count in self.bases.get(t_pt1_base, {}).items()}
                e_pt2_modifiers = {
                    self.modifiers[self.modifier_ids[i_mod]]: i_count
                    for i_mod, i_count in self.bases.get(t_pt2_base, {}).items()}
                
                if (
                    d_score_diff <= e_MIN_PT_DIFF['merge pt2 words found with pt1'] and
                    all((word,) in e_pt1_modifiers or
                        c_UNIMPORTANT_PTYPE_WORDS
                        for word in t_pt2)
                ):
                    s_reason = 'merge pt2 words found with pt1'
                    
                    t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                       b_pt1_is_base=None, s_merge_type='', s_between='',)
                elif (
                    d_score_diff <= e_MIN_PT_DIFF['merge pt1 words found with pt2'] and
                    all((word,) in e_pt2_modifiers or
                        c_UNIMPORTANT_PTYPE_WORDS
                        for word in t_pt1)
                ):
                    s_reason = 'merge pt1 words found with pt2'
                    t_pt = self._merge(t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
                                       b_pt1_is_base=None, s_merge_type='', s_between='',)
                else:
                    t_pt, s_reason = t_pt1, 'no merge'
                    
        return (t_pt, s_reason)
        
    def _merge(self, t_pt1, t_pt2, t_pt1_base, t_pt1_modifier, t_pt2_base, t_pt2_modifier,
           b_pt1_is_base=None, s_merge_type='modifiers', s_between='',):
            c_pt1_words = set(t_pt1)
            for s_word in t_pt1:
                s_plural = pluralize(s_word)
                s_singular = singularize(s_word)
                if s_plural not in c_pt1_words: c_pt1_words.add(s_plural)
                if s_singular not in c_pt1_words: c_pt1_words.add(s_singular)
            c_pt2_words = set(t_pt2)
            for s_word in t_pt2:
                s_plural = pluralize(s_word)
                s_singular = singularize(s_word)
                if s_plural not in c_pt2_words: c_pt2_words.add(s_plural)
                if s_singular not in c_pt2_words: c_pt2_words.add(s_singular)
            
            if b_pt1_is_base is None:
                if t_pt1[-1] == t_pt2[0]:
                    #print('pt1 end overlaps pt2 start', t_pt1, t_pt2)
                    b_pt1_is_base = False
                elif t_pt2[-1] == t_pt1[0]:
                    #print('pt2 end overlaps pt1 start', t_pt1, t_pt2)
                    b_pt1_is_base = True
                elif t_pt1_base[-1] in PTYPE_INDICATORS:
                    #print('t_pt1_base[-1] in PTYPE_INDICATORS')
                    b_pt1_is_base = True
                elif t_pt2_base[-1] in PTYPE_INDICATORS:
                    #print('t_pt2_base[-1] in PTYPE_INDICATORS')
                    b_pt1_is_base = False
                else:
                    #print('unknown base', t_pt1, t_pt2)
                    b_pt1_is_base = True
            
            if s_merge_type == 'modifiers':
                if b_pt1_is_base:
                    if all(word in ADJECTIVES for word in t_pt2_modifier):
                        t_pt = tuple([word for word in t_pt2_modifier if not word in c_pt1_words]) + t_pt1
                    elif s_between:
                        t_pt = tuple([word for word in t_pt2_modifier if not word in c_pt1_words]) + (s_between,) + t_pt1
                    else:
                        t_pt = tuple([word for word in t_pt2_modifier if not word in c_pt1_words]) + t_pt1
                else:
                    if all(word in ADJECTIVES for word in t_pt1_modifier):
                        t_pt = tuple([word for word in t_pt1_modifier if not word in c_pt2_words]) + t_pt2
                    elif s_between:
                        t_pt = tuple([word for word in t_pt1_modifier if not word in c_pt2_words]) + (s_between,) + t_pt2
                    else:
                        t_pt = tuple([word for word in t_pt1_modifier if not word in c_pt2_words]) + t_pt2
        
            elif s_merge_type == 'bases':
                if b_pt1_is_base:
                    if s_between:
                        t_pt = t_pt1_modifier + t_pt2_base + (s_between,) + t_pt1_base
                    else:
                        t_pt = t_pt1_modifier + t_pt2_base + t_pt1_base
                else:
                    if s_between:
                        t_pt = t_pt1_modifier + t_pt1_base + (s_between,) + t_pt2_base
                    else:
                        t_pt = t_pt1_modifier + t_pt1_base + t_pt2_base
            
            else:
                if b_pt1_is_base:
                    if s_between:
                        t_pt = tuple([word for word in t_pt2 if not word in c_pt1_words]) + (s_between,) + t_pt1
                    else:
                        t_pt = tuple([word for word in t_pt2 if not word in c_pt1_words]) + t_pt1
                else:
                    if s_between:
                        t_pt = tuple([word for word in t_pt1 if not word in c_pt2_words]) + (s_between,) + t_pt2
                    else:
                        t_pt = tuple([word for word in t_pt1 if not word in c_pt2_words]) + t_pt2
            
            return t_pt


class SptSelector():
    def __init__(self, i_max_matches=-1, d_match_cutoff=0.0, s_db_path=""):
        self.spts_dict, self.gpc_dict = None, None
        self.spt_ids, self.gpc_ids = None, None
        self.spt_conf = None
        self.pt_cache, self.gpc_cache = {}, {}
        self.bases = None
        self.max_matches = i_max_matches
        self.match_cutoff = 0.0
        self.path = s_db_path if s_db_path else r"spt_gpcs.sqlite"
        
        self.load_dict()
        
    def __enter__(self):
        self.load()
    
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
    
    def load(self):
        self.load_dict()
    
    def close(self):
        if self.spts_dict is not None:
            self.spts_dict.close()
        if self.gpcs_dict is not None:
            self.gpcs_dict.close()
        self.pt_cache, self.gpc_cache = {}, {}
    
    def load_dict(self):
        self.spts_dict = FwDbDict.SqliteFile(
            s_path=_data_full_path(self.path),
            s_table="spts",
            s_name="spts",
            s_key="spt",
            a_fields=["spt_id", "base", "modifier", "num_gpcs", "num_datasets", "num_skus", "gpc_hhi", "gpcs"],
            a_field_types=["INTEGER", "TEXT", "TEXT", "TEXT", "INTEGER", "INTEGER", "INTEGER", "REAL", "TEXT"],
            a_field_req=[True, True, False, False, False],
            b_use_caching=False,
        )
        print("{} spts with known GPCs".format(len(self.spts_dict)))
        
        self.gpcs_dict = FwDbDict.SqliteFile(
            s_path=_data_full_path(self.path),
            s_table="gpcs",
            s_name="gpcs",
            s_key="gpc",
            a_fields=["gpc_id", "spts"],
            a_field_types=["INTEGER", "TEXT"],
            a_field_req=[True, False],
            b_use_caching=False,
        )
        print("{} known GPCs".format(len(self.gpcs_dict)))
    
        self.spt_ids = FwDbDict.get_sqlite_dict(
            s_path=_data_full_path("spt_gpcs.sqlite"),
            s_table="spts",
            s_keys="spt_id",
            a_values=["spt"],
        )
        self.gpc_ids = FwDbDict.get_sqlite_dict(
            s_path=_data_full_path("spt_gpcs.sqlite"),
            s_table="gpcs",
            s_keys="gpc_id",
            a_values=["gpc"],
        )
        
        e_sys_conf = FwDbDict.get_sqlite_dict(
            s_path=_data_full_path("attrconf.sqlite"),
            s_table="AttributesConfidence",
            s_keys="Value",
            a_values=["ConfidenceLevel", "QualityLevel"],
            b_lowercase_keys=True,
            b_tuple_keys=True,
        )
        self.spt_conf = {
            pt: (round(conf) / 100, round(qual) / 100)
            for pt, (conf, qual) in e_sys_conf.items()
        }
        
        self.bases = KnownPTypeBases()
    
    def _get_pt(self, spt):
        if isinstance(spt, int):
            spt = self.spt_ids.get(spt)
        
        if spt in self.pt_cache:
            return self.pt_cache[spt]
        else:
            tn_gpcs = self.spts_dict.get(spt)
            if not tn_gpcs: return None
            e_gpcs = {
                tuple(split_strip(self.gpc_ids[int(gpc)].lower(), '>')): freq
                for gpc, freq in json.loads(tn_gpcs.gpcs).items()}
            e_gpc_freqs = freq_dict_to_percent(e_gpcs)
            t_pt_gpc = (e_gpcs, e_gpc_freqs, tn_gpcs.num_datasets, tn_gpcs.num_skus, tn_gpcs.gpc_hhi)
            self.pt_cache[spt] = t_pt_gpc
            return t_pt_gpc
    
    def _get_gpc(self, gpc):
        if isinstance(gpc, int):
            gpc = self.gpc_ids.get(gpc)
        
        if gpc in self.gpc_cache:
            return self.gpc_cache[gpc]
        else:
            tn_spts = self.gpcs_dict.get(gpc)
            if not tn_spts: return None
            e_spts = {
                tuple(self.spt_ids[int(spt)].split()): ct
                for spt, ct in json.loads(tn_spts.spts).items()}
            self.gpc_cache[gpc] = e_spts
            return e_spts
    
    def score_pt_gpcs(self, s_pt, s_ven_gpc, a_gpcs=None, d_cutoff=None, b_is_spt=False, c_brand_words=None):
        if d_cutoff is None:
            d_cutoff = self.match_cutoff
        
        s_pt = s_pt.lower()
        t_pt = tuple(s_pt.split())
        
        if t_pt[-1] not in PTYPE_INDICATORS:
            d_indicator = 0.75
        elif len(t_pt) == 1:
            d_indicator = 0.5
        else:
            d_indicator = 1.0
        
        d_sys_conf, d_sys_qual = self.spt_conf.get(t_pt, (0, 0))
            
        d_brand_overlap = 0.5 if any(word in c_brand_words for word in t_pt) else 1.0
        
        a_pt_factors = [
            a_PT_LEN_SCORE[min(len(t_pt), 4) - 1],  # length in words
            a_PT_CHAR_LEN_SCORE[min(sum([len(word) for word in t_pt]), 7) - 1],  # length in characters
            d_indicator,  # pt ends with a ptype indicator such as "set", "kit", "case", etc
            d_brand_overlap,  # pt word also in brand
            d_sys_conf,  # confidence from the system
            d_sys_qual,  # quality/sufficiency from the system
        ]
        #d_pt_factors = sum(a_pt_factors) / len(a_pt_factors)
        d_pt_factors = reduce(lambda x, y: x*y, a_pt_factors) ** 0.5
        #print(s_pt, d_pt_factors, a_pt_factors)
        
        t_ven_gpc = tuple(split_strip(s_ven_gpc.lower(), '>'))
        
        if a_gpcs:
            c_gpcs = set()
            for gpc in a_gpcs:
                if isinstance(gpc, str): gpc = tuple(split_strip(gpc.lower(), '>'))
            c_gpcs.add(gpc)
        else:
            c_gpcs = set()
        
        # get the information about the pt (which gpcs it belongs in)
        t_pt_gpc = self._get_pt(s_pt)
        
        # get the information about the gpc (which spts it typically has)
        e_gpc_spts = self._get_gpc(s_ven_gpc)
        if e_gpc_spts:
            e_gpc_freqs = freq_dict_to_percent(e_gpc_spts)
            i_gpc_skus = sum(e_gpc_spts.values()) # how many times we've seen this gpc, and how important it is for the spt to be in it
            t_max_spt, d_max_spt = max(e_gpc_freqs.items(), key=itemgetter(1))  # most common spt in this gpc
            d_gpc_spt = e_gpc_freqs.get(t_pt, 0.0) / d_max_spt
            
            if t_pt == t_max_spt:
                d_gpc_score = 1.0
            elif i_gpc_skus < 10:  # we haven't seen this gpc very often so don't weight it as important
                d_gpc_score = d_gpc_spt ** 0.75
            else:
                d_gpc_score = d_gpc_spt ** 0.5
        else:
            d_gpc_score = 1.0
        
        a_scores = []
        if t_pt_gpc:
            d_hhi_score = t_pt_gpc[4] ** 0.5
            for t_gpc, i_count in t_pt_gpc[0].items():
                if b_is_spt or t_gpc in c_gpcs:  # if the gpc is in the system, give it a higher priority
                    d_freq_score = t_pt_gpc[1][t_gpc] ** 0.5
                else:  # otherwise use it raw
                    d_freq_score = t_pt_gpc[1][t_gpc] ** 0.75
                d_tax_similarity = taxonomy_similarity(list(t_ven_gpc), list(t_gpc)) if s_ven_gpc else 0.5
                d_score = ((d_hhi_score * d_freq_score) + d_tax_similarity + (d_pt_factors * 2) + d_gpc_score) / 5
                if d_score >= d_cutoff:
                    a_scores.append((s_pt, t_gpc, d_score))
        else:
            d_hhi_score = 1
            
        for gpc in c_gpcs:
            if t_pt_gpc and gpc in t_pt_gpc[0]: continue  # already done
            if not s_ven_gpc or gpc in c_MISSING:
                #print('no comparison', s_ven_gpc, gpc)
                d_tax_similarity = 0.5
            else:
                d_tax_similarity = taxonomy_similarity(list(t_ven_gpc), list(gpc))
                
            if t_pt_gpc:  # pt has been seen and vendor tax wasn't in it
                d_score = ((d_hhi_score * 0.5) + d_tax_similarity + (d_pt_factors * 2) + d_gpc_score) / 5
            else:  #pt hasn't been associated with gpcs
                d_score = (d_hhi_score + d_tax_similarity + (d_pt_factors * 2) + d_gpc_score) / 5
                
            if d_score >= d_cutoff:
                a_scores.append((s_pt, gpc, d_score))
        
        if b_is_spt and not a_scores:
            # spt should always be added even if there is no gpc match
            d_score = (d_hhi_score + (d_pt_factors * 2) + d_gpc_score) / 4
            if d_score >= d_cutoff:
                a_scores.append((s_pt, '', d_score))
            
        
        a_scores.sort(key=itemgetter(2), reverse=True)
        
        if self.max_matches > 0:
            return a_scores[:self.max_matches]
        else:
            return a_scores
    
    def check_all_pts(self, a_gpcs, a_spts, a_alts, a_rems, a_brands=None):
        ''' Check the SPT, alternate PT, and PT from remainder for most probability given
            the GPC from the vendor '''
        if not a_brands: a_brands = [None] * len(a_gpcs)
        z_pt_gpc = zip(a_gpcs, a_spts, a_alts, a_rems, a_brands)
        a_comparisons = [
            self.check_row_pts(
                s_gpc=gpc,
                s_spt=spt,
                s_alt=alt,
                s_rem=rem,
                c_brand_words=brand)
            for gpc, spt, alt, rem, brand in z_pt_gpc
        ]
        return a_comparisons
    
    def check_row_pts(self, s_gpc, s_spt='', s_alt='', s_rem='', d_match_cutoff=None, c_brand_words=None):
        a_scores = []
        s_gpc, s_spt, s_alt, s_rem = s_gpc.lower(), s_spt.lower(), s_alt.lower(), s_rem.lower()
        
        if s_spt:
            if ":" in s_spt:
                if s_spt: s_spt = re.sub(r"::+", ":", s_spt)
                s_spt, a_spt_gpcs = split_strip(s_spt, ':', 1, b_from_right=True)
                a_scores.extend([
                    t_pt_gpc + ('spt',)
                    for t_pt_gpc in self.score_pt_gpcs(
                        s_spt, s_gpc,
                        split_strip(a_spt_gpcs, ';'),
                        d_cutoff=d_match_cutoff,
                        b_is_spt=True,
                        c_brand_words=c_brand_words,)
                ])
            else:
                a_scores.extend([
                    t_pt_gpc + ('spt',)
                    for t_pt_gpc in self.score_pt_gpcs(
                        s_spt, s_gpc,
                        d_cutoff=d_match_cutoff,
                        b_is_spt=True,
                        c_brand_words=c_brand_words,)
                ])
        
        # alternates pts
        if not s_alt:
            pass
        elif ":" in s_alt:
            if s_alt: s_alt = re.sub(r"::+", ":", s_alt)
            a_alt = [
                split_strip(s_pt_gpc, ':', 1, b_from_right=True)
                for s_pt_gpc in split_strip(s_alt, '|')] if s_alt else []
            
            for s_alt_pt, s_alt_gpcs in a_alt:
                a_scores.extend([
                    t_pt_gpc + ('alt',)
                    for t_pt_gpc in
                    self.score_pt_gpcs(
                        s_alt_pt, s_gpc,
                        split_strip(s_alt_gpcs, ';'),
                        d_cutoff=d_match_cutoff,
                        c_brand_words=c_brand_words,)
                ])
        else:
            a_alt = split_strip(s_alt, ';')
            for s_alt_pt in a_alt:
                a_scores.extend(
                    [t_pt_gpc + ('alt',)
                    for t_pt_gpc in self.score_pt_gpcs(
                        s_alt, s_gpc,
                        d_cutoff=d_match_cutoff,
                        c_brand_words=c_brand_words,)
                ])
        
        # pts in remainder
        if not s_rem:
            pass
        elif ":" in s_rem:
            if s_rem: s_rem = re.sub(r"::+", ":", s_rem)
            a_rem = [
                split_strip(s_pt_gpc, ':', 1, b_from_right=True)
                for s_pt_gpc in split_strip(s_rem, '|')] if s_rem else []
            for s_rem_pt, s_rem_gpcs in a_rem:
                a_scores.extend([
                    t_pt_gpc + ('rem',)
                    for t_pt_gpc in
                    self.score_pt_gpcs(
                        s_rem_pt, s_gpc,
                        split_strip(s_rem_gpcs, ';'),
                        d_cutoff=d_match_cutoff,
                        c_brand_words=c_brand_words,)
            ])
        else:
            a_rem = split_strip(s_rem, ';') if s_rem else []
            for s_rem_pt in a_rem:
                a_scores.extend([
                    t_pt_gpc + ('rem',)
                    for t_pt_gpc in
                    self.score_pt_gpcs(
                        s_rem, s_gpc, 
                        d_cutoff=d_match_cutoff,
                        c_brand_words=c_brand_words,)
                ])
    
        a_scores.sort(key=itemgetter(2), reverse=True)
        
        if self.max_matches > 0:
            print('filter matches down to {}'.format(self.max_matches))
            return a_scores[:self.max_matches]
        else:
            return a_scores
    
    def unique_row_pts(self, a_row_pts):
        ''' Get the unique pts for a row using the sorted list of tuples
        returned from check_row_pts.
        If there are copies of the same pt in different taxonomies,
        pick the one with the best score. '''
        
        if not a_row_pts: return []
        a_unique_pts = []
        c_pts = set()
        for t_pt in a_row_pts:
            if t_pt[0] in c_pts: continue
            c_pts.add(t_pt[0])
            a_unique_pts.append(t_pt)
        return a_unique_pts

    def pick_row_pt(self, s_gpc, **kwargs):
        ''' Picking the best pt '''
        a_scores = self.check_row_pts(s_gpc, **kwargs)
        a_scores = [t_score for t_score in a_scores
            if t_score[3] == 'spt' or t_score[3] not in c_MISSING]
        
        return max(a_scores, key=itemgetter(2))
        
    def get_final_pt(self, title, s_spt, s_alt, s_rem, s_gpc, brand=None):
        if isinstance(title, str):
            s_clean_title = clean_text(title, b_lowercase=True, b_punctuation=True)
            a_title = word_tokenize(s_clean_title, stop_words=None)
        else:
            a_title = title[:]
        
        if brand:
            if isinstance(brand, str):
                c_brand_words = set(word_tokenize(clean_text(brand, b_lowercase=True, b_punctuation=True)))
            elif isinstance(brand, (list, tuple)):
                c_brand_words = set(word_tokenize(brand))
            elif isinstance(brand, set):
                c_brand_words = brand
        else:
            c_brand_words = set()
                
        #print("spt: '{}', alt: '{}', gpc: '{}".format(s_spt, s_alt, s_gpc))
        a_pts = self.check_row_pts(
            s_spt=s_spt, s_alt=s_alt, s_rem=s_rem, s_gpc=s_gpc,
            d_match_cutoff=0.0, c_brand_words=c_brand_words,
        )
        a_pts = self.unique_row_pts(a_pts)
        
        if not a_pts:
            s_reason = 'no pt data in db'
            s_final = s_spt
        elif len(a_pts) <= 1:  # no merging
            s_reason = 'no merging'
            s_final = a_pts[0][0]
        else:
            a_best_pts = [(pt, score) for pt, gpc, score, source in a_pts]
            t_spt, s_reason = self.bases.merge_pts(
                a_title,
                a_best_pts[0][0],
                a_best_pts[1][0],
                a_best_pts[0][1] - a_best_pts[1][1]
            )
            s_final = ' '.join(t_spt)
        
        return s_final, s_reason


def convert_tree_node_count_to_percent(e_taxonomies, e_level_counts, e_parents):
    ''' Convert the node count for each taxonomy level to be a percentage of the count for that level '''
    e_taxonomy_percents = {}
    for node, i_count in e_taxonomies.items():
        if node in e_parents:
            e_taxonomy_percents[node] = i_count / e_taxonomies.get(e_parents[node], i_count)
        else:
            e_taxonomy_percents[node] = i_count / e_level_counts.get(1, i_count)
    return e_taxonomy_percents


def taxonomy_probabilities(taxonomy, e_taxonomy_percents):
    if isinstance(taxonomy, str):
        t_nodes = tuple(split_strip(taxonomy.lower(), '>'))
    else:
        t_nodes = taxonomy

    a_probabilities = []
    for i_level in range(1, len(t_nodes) + 1):
        a_probabilities.append(e_taxonomy_percents.get(t_nodes[:i_level], 1.0))
    return a_probabilities


def get_node_importance(d_factor=0.3333, i_nodes=7):
    a_node_importance, d_remaining_importance = [], 1
    for i in range(i_nodes):
        d_importance = d_remaining_importance * d_factor
        a_node_importance.append(d_importance)
        d_remaining_importance -= d_importance
    if d_factor > 0.5:
        a_node_importance[-1] += d_remaining_importance
    else:
        a_node_importance[0] += d_remaining_importance
    return a_node_importance


def get_taxonomy_confidence(a_coverage):
    a_confidence = []
    for i_node, d_coverage in enumerate(a_coverage):
        a_confidence.append(
            (d_coverage**0.25) * a_node_probability_weight[i_node]
        )
    return sum(a_confidence) / sum(a_node_probability_weight[:len(a_coverage)])

