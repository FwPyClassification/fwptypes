'''
Test cases for FwPTypes/mc_ptypes
'''
from __future__ import absolute_import
import pyximport
import math
import pandas as pd
import FwText, FwFreq
from FwPTypes import mc_ptypes
pyximport.install()


class TestMCPTNodeNgrams():
    def test_empty(self):
        a_ngrams = mc_ptypes.mcpt_node_ngrams([])
        assert a_ngrams == []

    def test_normal(self):
        a_ngrams = mc_ptypes.mcpt_node_ngrams(['this', 'is', 'a', 'test'])
        assert a_ngrams == ['this is a test', 'is a test', 'a test', 'test']
    

class TestMCPTListNgrams():
    def test_empty(self):
        a_ngrams = mc_ptypes.mcpt_list_ngrams(s_prefix='', a_list=[], s_suffix='')
        assert a_ngrams == []

    def test_normal(self):
        a_ngrams = mc_ptypes.mcpt_list_ngrams(
            s_prefix='car',
            a_list=['engine', 'radiator'], s_suffix='parts'
        )
        print(a_ngrams)
        assert a_ngrams == ['car engine parts', 'engine parts', 'parts',
                            'car radiator parts', 'radiator parts', 'parts']
    

class TestMCPTNgrams():
    ''' this validates mcpt_ngrams and mcpt_node_potential_ptypes '''
    def test_empty(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('')

    def test_plain(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('Aquarium Air Pumps')
        assert a_ngrams == ['Aquarium Air Pumps', 'Air Pumps', 'Pumps']

    def test_one_ampersand(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('Coats & Jackets')
        assert a_ngrams == ['Coats & Jackets', 'Coats', 'Jackets']

    def test_backslashes(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('Coats\Jackets\Sweaters')
        assert a_ngrams == ['Coats\\Jackets\\Sweaters', 'Coats', 'Jackets', 'Sweaters',
                            'Coats & Sweaters', '& Sweaters', 'Jackets & Sweaters']

    def test_comma_list(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('Collars, Leashes & Harnesses')
        assert a_ngrams == ['Collars, Leashes & Harnesses', 'Collars', 'Leashes', 'Harnesses']

    def test_multiword_phrase(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('Pet ID Tags & Lights')
        assert a_ngrams == ['Pet ID Tags & Lights', 'Pet ID Tags', 'ID Tags', 'Tags', 'Pet Lights', 'Lights']

    def test_multiword_comma_list(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('Pet ID Tags, Lights, & Monitors')
        assert a_ngrams == ['Pet ID Tags, Lights, & Monitors', 'Pet ID Tags', 'ID Tags', 'Tags', 'Pet Lights',
                            'Lights', 'Pet Monitors', 'Monitors']

    def test_multiword_ampersand(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('Aquarium & Fish Supplies')
        assert a_ngrams == ['Aquarium & Fish Supplies', 'Aquarium Supplies', 'Supplies', 'Fish Supplies']

    def test_backslash_suffix(self):
        a_ngrams = mc_ptypes.mcpt_ngrams('mouse\wrist pads - computers')
        assert a_ngrams == ['mouse\\wrist pads - computers', 'mouse', 'wrist pads computers', 'pads computers',
                            'computers', 'mouse pads - computers', 'pads - computers', '- computers',
                            'wrist pads - computers', 'computer mouse\\wrist pads', 'computer mouse',
                            'wrist pads', 'pads', 'computer mouse pads', 'mouse pads', 'computer wrist pads',
                            'mouse\\wrist pads', 'mouse', 'wrist pads', 'pads', 'mouse pads']


# need unit tests for whole dataset mc ptype analysis class MCPType
