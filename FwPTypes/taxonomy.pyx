﻿import re
from collections import defaultdict
from FwText import split_strip

__all__ = [
    "prep_taxonomy_string", "get_taxonomy_counts",
    "get_tree_taxonomy_counts", "taxonomy_similarity"]

cdef list a_MISSING_SCORE = [round(1 - ((1 / i) - 0.1), 4) for i in range(1, 11)]
cdef list a_MISMATCH_SCORE = [1 - round(2 - i ** 0.3, 4) for i in range(1, 11)]
cdef list a_MISSING_DELTA_SCORE = [round(y - x, 4) for x, y in zip(a_MISSING_SCORE, a_MISSING_SCORE[1:])]
cdef list a_MISMATCH_DELTA_SCORE =  [round(y - x, 4) for x, y in zip(a_MISMATCH_SCORE, a_MISMATCH_SCORE[1:])]
cdef set c_EMPTY = {
    '', 'n/a', 'na', 'none', 'missing', 'empty', 'unknown',
    tuple(), ('',), ('n/a',), ('na',), ('none',), ('missing',), ('empty',), ('unknown',),
}
re_gt = re.compile(r"(&gt;|(?<=\s)[|\\:](?=\s))", flags=re.I)


cpdef str prep_taxonomy_string(str taxonomy):
    ''' make sure the taxonomy string is ready for processing '''
    taxonomy = re_gt.sub('>', taxonomy)
    return taxonomy


cpdef dict get_taxonomy_counts(list a_taxonomies, bint b_use_tuples=True):
    ''' Count the frequencies of every node level of the taxonomies in the list '''

    cdef dict e_node_freqs = {}
    cdef int i_level
    cdef tuple t_node
    cdef str s_taxonomy, s_node
    cdef list a_nodes
    
    if b_use_tuples:
        for s_taxonomy in a_taxonomies:
            a_nodes = split_strip(s_taxonomy.lower(), '>')
            for i_level in range(1, len(a_nodes)):
                t_node = tuple(a_nodes[:i_level])
                if t_node not in e_node_freqs:
                    e_node_freqs[t_node] = 1
                else:
                    e_node_freqs[t_node] += 1
    else:
        for s_taxonomy in a_taxonomies:
            a_nodes = split_strip(s_taxonomy.lower(), '>')
            for i_level in range(1, len(a_nodes)):
                s_node = ' > '.join(a_nodes[:i_level])
                if s_node not in e_node_freqs:
                    e_node_freqs[s_node] = 1
                else:
                    e_node_freqs[s_node] += 1
                
    return e_node_freqs


cpdef tuple get_tree_taxonomy_counts(list a_taxonomies, bint b_use_tuples=True):
    ''' Count the frequencies of every node level of the taxonomies in the list '''
    cdef dict e_node_freqs, e_level_counts, e_parents
    cdef str s_taxonomy, s_node, s_parent
    cdef tuple t_nodes, t_node, t_parent
    cdef int i_level
    
    e_node_freqs = {}
    e_level_counts = {}
    e_parents = {}
    
    if b_use_tuples:
        for s_taxonomy in a_taxonomies:
            t_nodes = tuple(split_strip(s_taxonomy.lower(), '>'))
            t_parent = None
            for i_level in range(1, len(t_nodes) + 1):
                t_node = t_nodes[:i_level]
                if t_node not in e_node_freqs:
                    e_node_freqs[t_node] = 1
                else:
                    e_node_freqs[t_node] += 1
                if i_level not in e_level_counts:
                    e_level_counts[i_level] = 1
                else:
                    e_level_counts[i_level] += 1
            
                if t_parent and t_node not in e_parents:
                    e_parents[t_node] = t_parent
                    
                t_parent = t_node
    else:
        for s_taxonomy in a_taxonomies:
            t_nodes = tuple(split_strip(s_taxonomy.lower(), '>'))
            s_parent = None
            for i_level in range(1, len(t_nodes) + 1):
                s_node = ' > '.join(t_nodes[:i_level])
                if s_node not in e_node_freqs:
                    e_node_freqs[s_node] = 1
                else:
                    e_node_freqs[s_node] += 1
                if i_level not in e_level_counts:
                    e_level_counts[i_level] = 1
                else:
                    e_level_counts[i_level] += 1
                
                if s_parent and s_node not in e_parents:
                    e_parents[s_node] = s_parent
                
                s_parent = s_node
                
    return e_node_freqs, e_level_counts, e_parents


cpdef float taxonomy_similarity(list a_tax1, list a_tax2):
    ''' Try to compare the similarity of two taxonomies in a way that
        weights mismatches more than missing, and problems closer to the root
        as worse than those toward the end. '''
    cdef list a_mismatched, a_missing
    cdef int i_max, i_min, i_nd, i_len1, i_len2
    cdef str s_nd1, s_nd2
    cdef float d_missing, d_mismatched
    
    i_len1, i_len2 = len(a_tax1), len(a_tax2)
    i_max, i_min = max(i_len1, i_len2), min(i_len1, i_len2)
    if i_len1 > i_len2:
        a_tax2 += [''] * (i_len1 - i_len2)
    elif i_len2 > i_len1:
        a_tax1 += [''] * (i_len2 - i_len1)
    
    a_mismatched, a_missing = [], []
    for i_nd, (s_nd1, s_nd2) in enumerate(zip(a_tax1, a_tax2)):
        if not s_nd1 or not s_nd2:
            a_missing.append(i_nd)
        elif s_nd1 != s_nd2:
            a_mismatched.append(i_nd)
    
    if not a_missing:
        d_missing = 1.0
    elif len(a_missing) == 1:
        d_missing = a_MISSING_SCORE[a_missing[0]]
    else:
        d_missing = a_MISSING_SCORE[a_missing[0]] - sum([a_MISSING_DELTA_SCORE[i] for i in a_missing[1:]])
        
    if not a_mismatched:
        d_mismatched = 1.0
    elif len(a_mismatched) == 1:
        d_mismatched = a_MISMATCH_SCORE[a_mismatched[0]]
    else:
        d_mismatched = a_MISMATCH_SCORE[a_mismatched[0]] - sum([a_MISMATCH_DELTA_SCORE[i] for i in a_mismatched[1:]])
        
    d_missing = round(d_missing, 2)        
    d_mismatched = round(d_mismatched, 2)
    return d_mismatched * d_missing
