﻿import os
import setuptools
from setuptools.command.install import install
import re
import sys

module_path = os.path.join(os.path.dirname(__file__), 'FwPTypes/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall("\d+\.\d+\.\d+", version_line)[0]


class CustomInstall(install):
    """
    @description: Installs external FindWatt dependencies before installing
    the current package.
    """
    def run(self):
        self.install_fw_dependencies()
        install.run(self)
        
    def install_fw_dependencies(self):
        if sys.version_info.major == 2:
            os.system("pip install git+https://bitbucket.org/FindWatt/fwtext")
            os.system("pip install git+https://bitbucket.org/FindWatt/fwfreq")
            os.system("pip install git+https://bitbucket.org/FindWatt/fwdbdict")
            os.system("pip install git+https://bitbucket.org/FwPyClassification/fwdictionary")
        elif sys.version_info.major == 3:
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwtext")
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwfreq")
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwdbdict")
            os.system("pip3 install git+https://bitbucket.org/FwPyClassification/fwdictionary")

setuptools.setup(
    name="FwPTypes",
    version=__version__,
    url="https://bitbucket.org/FwPyClassification/fwptypes",

    author="FindWatt",

    description="",
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={'': ["*.pyx", "data/*.sqlite"]},
    py_modules=['FwPTypes'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "pandas",
        "numpy",
        "scipy",
    ],
    cmdclass={'install': CustomInstall}
)
