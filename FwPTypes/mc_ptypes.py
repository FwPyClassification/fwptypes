import operator, os, re
from string import punctuation
import FwText, FwFreq
from FwDictionary import FwDictionary


__all__ = [
    'MCPType', 'mcpt_ngrams', 'mcpt_node_potential_ptypes',
    "NON_PTYPE_WORDS", "POOR_PTYPE_WORDS",
    'mcpt_node_ngrams', 'mcpt_list_ngrams']


DICT = FwDictionary()
STOP_WORDS_DICT = FwDictionary("fw_stop_words")
ACCESSORY_DICT = FwDictionary("fw_accessory_words")
QTY_WORDS_DICT = FwDictionary("fw_quantity_words")
CONJUNCTIONS_DICT = FwDictionary("fw_conjunction_words")
PREPOSITIONS_DICT = FwDictionary("fw_preposition_words")
ABBREVIATIONS_DICT = FwDictionary("fw_abbreviation_words")
ADJECTIVE_DICT = FwDictionary("fw_adjective_words")
LIFESTAGE_DICT = FwDictionary("fw_lifestage_words")
COLOR_DICT = FwDictionary("fw_colors")
                       
NON_PTYPE_WORDS = set(list(STOP_WORDS_DICT.vocabulary) +
                      list(QTY_WORDS_DICT.vocabulary) +
                      list(CONJUNCTIONS_DICT.vocabulary) +
                      list(PREPOSITIONS_DICT.vocabulary) +
                      list(ABBREVIATIONS_DICT.vocabulary) +
                      list(LIFESTAGE_DICT.vocabulary) +
                      list(punctuation))

POOR_PTYPE_WORDS = set(list(ADJECTIVE_DICT.vocabulary) +
                       list(ACCESSORY_DICT.vocabulary) +
                       list(COLOR_DICT.vocabulary))


class MCPType():
    ''' Discover PTypes by looking for terms in the Merchant Category or Google Taxonomy,
        that also appear in the Title and Description.
        Terms that appear closer to the root node have more weight as long as they
         appear in the Title or Description. '''
    def __init__(self, df, s_id_col, s_mc_col, s_pn_col, s_desc_col, a_invalid_token_cols):
        self.ids, self.mcs = list(df[s_id_col]), list(df[s_mc_col])
        self.pns = [str(x) if str(x) != 'nan' else '' for x in df[s_pn_col]]
        self.descs = [str(x) if str(x) != 'nan' else '' for x in df[s_desc_col]]
        self.invalid_token_cols = [s_col for s_col in a_invalid_token_cols if s_col in df.columns]
        
        self.df = pd.DataFrame(list(zip(self.ids, self.mcs, self.pns, self.descs)),
                               columns=['ID', 'MC', 'PN', 'DESC'], index=df.index)
        self.df = pd.merge(self.df, df[self.invalid_token_cols], left_index=True, right_index=True, how='inner')
        
        self.mc_product_counts = {}
        self.taxonomy = {}
        self.mcpts = {}

    def find_ptypes(self):
        ''' Find probable MC PTypes for each group of products in the dataset sharing the same MC. '''
        
        # loop through to look at each group of products sharing the same MC
        for s_mc, pd_mc in self.df.groupby(['MC']):
            # count products and create tree taxonomy
            self._count_products_and_create_taxonomy(s_mc, len(pd_mc))
            
            # create a list of tokens to remove from MC before creating potential ptypes
            e_invalid_tokens = set([str(val) for s_col in self.invalid_token_cols for val in pd_mc[s_col]])
            
            # find probable ptypes
            a_grp_mcpt = self._mc_group_ptypes(s_mc, list(pd_mc.PN), list(pd_mc.DESC), e_invalid_tokens)
            
            self.mcpts[s_mc] = a_grp_mcpt

    def _count_products_and_create_taxonomy(self, s_mc, i_products):
        ''' Count products and create tree taxonomy '''
        # split the MC into nodes
        a_mc = FwText.split_strip(s_mc, '>')
        for i_len in range(len(a_mc)):
            s_node = ' > '.join(a_mc[:i_len])
            # update the product count for the node
            self.mc_product_counts[s_node] = self.mc_product_counts.get(s_node, 0) + i_products
            # add node to tree taxonomy
            if i_len == 0: e_parent = self.taxonomy
            if s_node not in e_parent: e_parent[s_node] = {}
            e_parent = e_parent[s_node]

    def _mc_group_ptypes(self, s_mc, a_pns, a_descs, e_invalid_tokens):
        ''' Find the most probable ptypes for a group of products with same MC. '''
        # remove invalid tokens (like brand or color) being creating possible ptypes
        for s_token in e_invalid_tokens: s_mc.replace(s_token, '')
        a_mc = FwText.split_strip(s_mc, '>')
        
        a_mcpts, a_potential, a_probable = [], [], []
        if not s_mc: return (a_mcpts, a_probable)
        e_depth_scores = {}
        for i_depth, s_node in enumerate(a_mc):
            a_node_potential = mcpt_node_potential_ptypes(s_node)
            if a_node_potential:
                for s_pt in a_node_potential:
                    if s_pt in e_depth_scores:
                        e_depth_scores[s_pt] += (1 - ((i_depth) * 0.1))
                    else:
                        e_depth_scores[s_pt] = 1 - ((i_depth) * 0.1)
            a_potential.extend(a_node_potential)
        
        e_potential = FwFreq.freq(a_potential, remove_empty=True)
        
        e_title_count = self._mcpt_in_texts(e_potential, a_pns)
        e_desc_count = self._mcpt_in_texts(e_potential, a_descs)
        
        e_possible = FwFreq.add_freq_dicts(e_title_count, e_desc_count)
        
        d_pt_hhi = FwFreq.herfindahl_freqs_dict(e_possible)
        
        a_probable = self._add_scores_and_sort(FwFreq.freq_dict_to_percent(e_possible),
                                               e_depth_scores, d_pt_hhi, len(a_pns))
        return a_probable

    def _add_scores_and_sort(self, e_potential, e_depth_scores, d_pt_hhi, i_grp_count):
        ''' Create an array of the potential ptypes and sort by probability
            as calculated with depth score. '''
        if not e_potential: return ("", 0, 0, 0, d_pt_hhi, i_grp_count)
        a_probable = []
        for s_pt, d_freq in e_potential.items():
            if d_freq > 0:
                d_spaces = 1 + len(re.findall(' +', s_pt)) * 0.1
                d_depth = e_depth_scores.get(s_pt, 0)
                a_probable.append([s_pt, d_freq, d_depth, d_freq * d_depth * d_spaces, d_pt_hhi, i_grp_count])
        
        return sorted(a_probable, key=operator.itemgetter(3), reverse=True)

    def _mcpt_in_texts(self, e_mcpts, a_texts):
        ''' Go through each potential ptype and count its coverage in the text. '''
        e_mcpt_in_texts = {}
        for s_pt, i_count in e_mcpts.items():
            e_mcpt_in_texts[s_pt] = 0
            for s_text in a_texts:
                if FwText.word_in_text(s_text, s_pt, position_function='word'):
                    e_mcpt_in_texts[s_pt] += 1
        return e_mcpt_in_texts

# end MCPType class


def mcpt_ngrams(mc, e_invalid_tokens={}, b_remove_with=False, b_remove_end_attribute=True):
    if isinstance(mc, str):
        for s_tok in e_invalid_tokens: mc.replace(s_tok, '')
        a_mc = FwText.split_strip(mc, '>')
    elif isinstance(mc, (list, tuple)):
        if isinstance(mc[0], str):
            a_mc = [s_node.replace(s_tok, '') for s_node in mc for s_tok in e_invalid_tokens]
        elif isinstance(mc[0], (list, tuple)):
            a_mc = [' '.join(a_node) for a_node in mc]
            a_mc = [s_node.replace(s_tok, '') for s_node in a_mc for s_tok in e_invalid_tokens]
    else:
        return []
    if not a_mc:
        return []
    
    a_mcpts = []
    for i_depth, s_node in enumerate(a_mc):
        a_mcpts.extend(mcpt_node_potential_ptypes(s_node, b_remove_with=b_remove_with, b_remove_end_attribute=True))
        if b_remove_end_attribute and '-' in s_node:
            a_mcpts.extend(mcpt_node_potential_ptypes(s_node[:s_node.index('-')].strip(), b_remove_with=b_remove_with,
                           b_remove_end_attribute='False'))
        
    return a_mcpts
    

def mcpt_node_potential_ptypes(s_node, b_remove_with=False, b_remove_end_attribute=False):
    ''' split a node of the merchant category into ngrams of potential ptypes.
        Examples:
            'Aquarium Air Pumps' ['Aquarium Air Pumps', 'Air Pumps', 'Pumps']
            'Coats & Jackets' ['Coats & Jackets', 'Coats', 'Jackets']
            'Coats\Jackets\Sweaters' ['Coats ', 'Jackets', 'Sweaters']
            'Collars, Leashes & Harnesses' ['Collars, Leashes & Harnesses', 'Collars', 'Leashes', 'Harnesses']
            'Pet ID Tags & Lights' ['Pet ID Tags & Lights', 'Pet ID Tags', 'ID Tags', 'Tags', 'Pet Lights', 'Lights']
            'Pet ID Tags, Lights, & Monitors' ['Pet ID Tags, Lights, & Monitors', 'Pet ID Tags',
            'ID Tags', 'Tags', 'Pet Lights', 'Pet Monitors', 'Lights', 'Monitors']
            'Tees, Jerseys, & Dresses' ['Tees, Jerseys, & Dresses', 'Tees', 'Jerseys', 'Dresses']
            'Aquarium & Fish Supplies' ['Aquarium & Fish Supplies', 'Aquarium Supplies', 'Fish Supplies', 'Supplies']
            'mouse\wrist pads - computers' ['mouse\wrist pads computers', 'mouse\wrist pads', 'mouse pads',
            'mouse pads computers', 'wrist pads', 'wrist pads computers', 'pads computers', 'computers',
            'computer mouse\wrist pads', 'computer mouse\wrist', 'computer mouse', 'computer wrist']
    '''
    s_element = s_node
    s_prefix, s_suffix = '', ''
    
    if b_remove_with:  # optionally split off " for ____" and "with _____" phrases from node
        if ' for ' in s_element: s_element = s_element.split(' for ')[0]
        if ' with ' in s_element: s_element = s_element.split(' with ')[0]
    
    if b_remove_end_attribute:  # optionally split off common attributes from the end if they are denoted
        # with 'word - attribute'
        l_dash = s_element.find(' - ')
        if l_dash > 0:
            s_suffix = s_element[l_dash + 2:].strip()
            if re.search(r"[0-9]", s_suffix): s_element = s_element[:l_dash]
            elif s_suffix.lower() in POOR_PTYPE_WORDS or s_suffix.lower() in NON_PTYPE_WORDS:
                s_element = s_element[:l_dash]
            s_suffix = ''
    
    a_potential_ptypes = [s_element]
    a_slashes = [i_pos for i_pos, s_char in enumerate(s_element) if s_char == r'\\'[:-1]]
    if len(a_slashes) >= 1:  # and not r'&' in s_element:
        # split backslash delimited list
        for s_sub_element in s_element.split(r'\\'[:-1]):
            a_list = FwText.word_tokenize(s_sub_element, stop_words=None)
            while a_list and not DICT.__contains__(a_list[-1]):
                a_list.pop()
            a_potential_ptypes.extend(mcpt_node_ngrams(a_list))
        
    if re.search(r"(&| and |\\)", s_element):
        # special handling for nodes with list indicators such as commas, ampersands, or "and"
        s_element = re.sub(r"( +and +| *\\ *)", " & ", s_element)
        
        l_comma = s_element.find(',')
        l_and = s_element.find('&')
        if l_comma > 0:
            l_space = s_element.find(' ', 0, l_comma - 1)
            if l_space > 0:
                s_prefix = s_element[:l_space]
                s_element = s_element[l_space + 1:]
        elif l_and > 1:
            l_space = s_element.find(' ', 0, l_and - 2)
            if l_space > 0:
                s_prefix = s_element[:l_space]
                s_element = s_element[l_space + 1:]
            
        l_and = s_element.find('&')
        if len(s_element) > l_and:
            l_space = s_element.find(' ', l_and + 2)
            if l_space > 0:
                s_suffix = s_element[l_space + 1:].strip()
                s_element = s_element[:l_space]
        
        a_before = FwText.split_strip(s_element[:l_and], ',')
        a_after = FwText.split_strip(s_element[l_and + 1:], ',')
        a_list = [s_word for s_word in a_before + a_after if s_word]
        a_potential_ptypes.extend(mcpt_list_ngrams(s_prefix.strip(), a_list, s_suffix.strip()))
        
    else:
        # normal node. split into words and create ngrams from last word
        a_list = FwText.word_tokenize(s_element, stop_words=None)
        while a_list and not DICT.__contains__(a_list[-1]):
            a_list.pop()
        a_potential_ptypes.extend(mcpt_node_ngrams(a_list))
    
    # filter out potential ptypes that are too short, are stop words, are collection words
    a_probable_ptypes = []
    for s_pptype in a_potential_ptypes:
        if len(s_pptype) < 3: continue  # if the word is too short to be a valid PType
        elif s_pptype in a_probable_ptypes: continue
        elif s_pptype.find(' ') == -1:  # if there is only one word, run checks against lists
            if s_pptype not in NON_PTYPE_WORDS:
                a_probable_ptypes.append(s_pptype)
            elif re.search("[0-9]", s_pptype):
                pass
        else:
            a_probable_ptypes.append(s_pptype)
    
    if ' - ' in s_node:
        s_base_node, s_modifier = s_node.split(' - ', 1)
        s_switched_node = '{} {}'.format(FwText.singularize(s_modifier), s_base_node)
        a_switched_ptypes = mcpt_node_potential_ptypes(s_switched_node, b_remove_with=False,
                                                       b_remove_end_attribute=False)
        a_probable_ptypes.extend([pt for pt in a_switched_ptypes if pt not in a_probable_ptypes])
        
    return a_probable_ptypes
    

def mcpt_node_ngrams(a_words, i_max_length=4):
    ''' pass in list of node words and return the ngrams, anchored on end word '''
    if not a_words: return []
    a_ngrams = []
    for i_size in reversed(range(1, min(i_max_length, len(a_words)) + 1)):
        a_ngrams.append(' '.join(a_words[-i_size:]))
    
    return a_ngrams
    

def mcpt_list_ngrams(s_prefix, a_list, s_suffix):
    ''' pass in arrays of list of prefix words, main list words, and suffix words, and return the ngrams '''
    a_ngrams = []
    if a_list:
        for s_piece in a_list:
            a_words = FwText.word_tokenize("{} {} {}".format(s_prefix, s_piece, s_suffix).strip(),
                                           remove_punct=False, stop_words=None)
            while a_words and (a_words[-1] in NON_PTYPE_WORDS or not DICT.__contains__(a_words[-1])):
                a_words.pop()
            
            if a_words: a_ngrams.extend(mcpt_node_ngrams(a_words))
    else:
        a_words = FwText.word_tokenize("{} {}".format(s_prefix, s_suffix).strip(), remove_punct=False, stop_words=None)
        if a_words:
            a_ngrams.extend(mcpt_node_ngrams(a_words))
    return a_ngrams
