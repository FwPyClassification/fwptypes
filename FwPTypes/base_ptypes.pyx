﻿import math, operator, re
from collections import defaultdict, Counter
import pandas as pd
from FwText import split_strip, end_grams, word_tokenize, singularize_tuple, pluralize_tuple, NLTK_STEMMER
from FwFreq import freq, freq_dict_to_lower, freq_merge_stems, herfindahl_freqs_dict, sort_freqs
from FwDictionary import FwDictionary

__all__ = ["BasePTypes", "PTypeEnhancements"]

c_TRIM = FwDictionary('fw_ptype_trim_words').vocabulary
c_NOT_WHOLE = FwDictionary('fw_ptype_not_whole').vocabulary
c_NOT_INCLUDE = FwDictionary('fw_ptype_bad_words').vocabulary
cdef set c_QUANTITY = FwDictionary('fw_quantity_words').vocabulary
c_ACCESSORY = FwDictionary('fw_accessory_words').vocabulary
c_SIZE = FwDictionary('fw_size_words').vocabulary
c_PREPOSITIONS = FwDictionary('fw_preposition_words').vocabulary
c_CONJUNCTIONS = FwDictionary('fw_conjunction_words').vocabulary
c_LIFESTAGES = FwDictionary('fw_lifestage_words').vocabulary
cdef set c_NUMS = set("0123456789")

cdef set c_EXCLUDE = set(
    list(c_TRIM) +
    list(c_NOT_WHOLE) +
    list(c_NOT_INCLUDE) +
    list(c_QUANTITY) +
    list(c_ACCESSORY) +
    list(c_SIZE) +
    list(c_PREPOSITIONS) +
    list(c_CONJUNCTIONS) +
    list(c_LIFESTAGES)
)

cdef set c_PT_TRIM_START = {'for', 'and', 'with', 'w', 'to'}
cdef set c_PT_SPLIT = {'for', 'with'}


cdef list _split_spt(list a_spt):
    ''' if the pt includes a phrase of what the product goes for/with
        then split and return just the base part '''
    cdef int idx
    cdef str word
    for idx, word in enumerate(a_spt):
        if idx > 0 and word in c_PT_SPLIT:
            return a_spt[:idx]
    return a_spt


class BasePTypes():
    ''' Look for the base ptypes
        These are end-grams that can't be reduced further
        If the frequencies of "Storage Container" and "Container" are the same then
        the first is the base ptype. But if "Container" is more frequent then it is base.
        Unless the last word is a quantity/container word
    '''
    
    def __init__(self, list a_spts, list a_alts=[], list a_rems=[], i_max_words=3, b_use_tuples=True, b_lower_case=True):
        cdef str spt, spts
        cdef list a_spt
        cdef list a_pts = [word_tokenize(spt, stop_words=False, convert_ampersand=True) for spt in a_spts]
        a_pts.extend(word_tokenize(spt, stop_words=False, convert_ampersand=True)
            for spts in a_alts + a_rems for spt in split_strip(spts, ';'))
        a_pts = [_split_spt(a_spt) for a_spt in a_pts]
        
        self._use_tuples, self._lower_case = b_use_tuples, b_lower_case
        self.rows = len(a_spts)
        
        self.bases, self.stems = {}, {}
        self.base_map, self.stem_map = {}, {}
        self.base_hhi, stem_hhi = 0.0, 0.0
        self.max_base, self.max_stem = 0, 0
        
        # get the end-grams that are the potential ptype bases
        cdef int i_ngram_len = i_max_words
        cdef dict e_ends = freq([
            end_grams(a_spt, i_max_length=i_max_words, return_tuples=True)
            for a_spt in a_pts])
            
        self.pts = a_pts
        self.ends = e_ends
        
        self._filter_ends()
        self._find_bases()
        self._accessory_bases()
        
        self._get_stems()
    
    def clear(self):
        self.pts = []
        
    def _split_spt(self, a_spt):
        return _split_spt(a_spt)
    
    def _filter_ends(self):
        ''' if the pt includes a phrase of what the product goes for/with
            then split and return just the base part '''
        cdef tuple t_pt
        cdef list a_low
        cdef str s_word
        
        for t_pt in list(self.ends.keys()):
            a_low = [s_word.lower() for s_word in t_pt]
            if a_low[0] in c_PT_TRIM_START:
                del self.ends[t_pt]
            elif all(s_word in c_EXCLUDE for s_word in a_low):
                del self.ends[t_pt]
            elif all(not re.search(r'[A-Za-z]+', s_word) for s_word in a_low):
                del self.ends[t_pt]
    
    def _accessory_bases(self):
        ''' if ptype looks like "xyz set", create base of "xyz" '''
        cdef tuple t_base, t_truncated
        cdef int i_count
        cdef str s_end
        
        for t_base, i_count in list(self.bases.items()):
            if len(t_base) == 1: continue
            s_end = t_base[-1].lower()
            if (s_end in c_QUANTITY or s_end in c_ACCESSORY):
                t_truncated = tuple(t_base[:-1])
                if t_truncated not in self.bases:
                    self.bases[t_truncated] = i_count
                else:
                    self.bases[t_truncated] += i_count
    
    def _find_bases(self):
        cdef dict e_bases = {}
        cdef tuple t_end_gram, t_base
        cdef int i_count
        cdef list at_ends
        
        self.bases = e_bases
        # sort end-ngrams longest to shortest
        at_ends = sorted(
            self.ends.items(),
            key=lambda t_end_gram: len(t_end_gram[0]),
            reverse=True)
        
        for t_end_gram, i_count in at_ends:
            self._check_base(t_end_gram, i_count)
        
        if not self._use_tuples:
            self.bases = {' '.join(t_base): i_count for t_base, i_count in self.bases.items()}
        
        if self._lower_case:
            self.bases = freq_dict_to_lower(self.bases)
                
        return self.bases
    
    
    def _check_base(self, t_end_gram, i_count):
        cdef int i_len, i_sub_len, i_sub_idx, i_sub_count, i_next_sub_count
        cdef tuple t_sub
        cdef str s_word
        cdef bint b_base_found
    
        i_len = len(t_end_gram)
        if i_len == 1:
            if not t_end_gram in self.bases:
                self.bases[t_end_gram] = i_count
            return
        
        a_subs = []
        for i_sub_len in reversed(range(1, i_len + 1)):
            t_sub = t_end_gram[-i_sub_len:] 
            if any(s_word not in c_EXCLUDE for s_word in t_sub):
                a_subs.append((t_sub, self.ends.get(t_sub, 0)))
        b_base_found = False
        for i_sub_idx, (t_sub, i_sub_count) in enumerate(a_subs):
            if b_base_found:
                try:
                    del self.ends[t_sub]
                except: pass
            else:
                i_next_sub_count = a_subs[i_sub_idx + 1][1] if i_sub_idx < len(a_subs) - 1 else 0
                if i_sub_count < i_next_sub_count:
                    try:
                        del self.ends[t_sub]
                    except: pass
                elif i_sub_count >= i_next_sub_count:
                    b_base_found = True
                    if not t_sub in self.bases:
                        self.bases[t_sub] = i_sub_count
                    
                    if self._use_tuples:
                        if self._lower_case:
                            self.base_map[tuple(s_word.lower() for s_word in t_end_gram)] = t_sub
                        else:
                            self.base_map[t_end_gram] = t_sub
                    else:
                        if self._lower_case:
                            self.base_map[' '.join(t_end_gram).lower()] = ' '.join(t_sub).lower()
                        else:
                            self.base_map[' '.join(t_end_gram)] = ' '.join(t_sub)
    
        self.base_hhi = herfindahl_freqs_dict(self.bases)
        self.max_base = max(self.bases.values()) if self.bases else 0
        
        for base in set(self.base_map.values()):
            if base not in self.base_map:
                self.base_map[base] = base
            singular, plural = singularize_tuple(base), pluralize_tuple(base)
            
            if singular not in self.base_map:
                self.base_map[singular] = base
            if plural not in self.base_map:
                self.base_map[plural] = base
        
        return self.bases
        
    def _get_stems(self):
        if not self.bases: self._find_bases()
        self.stems, self.stem_map = freq_merge_stems(self.bases, b_average=False)
        self.stem_hhi = herfindahl_freqs_dict(self.bases)
        self.max_stem = max(self.stems.values()) if self.stems else 0
        
        at_pts = [tuple([word.lower() for word in pt])
                  if isinstance(pt, (list, tuple))
                  else tuple(pt)
                  for pt in self.pts]
        self.stem_map.update(
            {singularize_tuple(pt): self.base_map[pt] for pt in at_pts if pt in self.base_map}
        )
        self.stem_map.update(
            {pluralize_tuple(pt): self.base_map[pt] for pt in at_pts if pt in self.base_map}
        )
        
        return self.stems
        
    def spt_confidence(self, spt):
        ''' Calculate the confidence of an SPT by looking at its base '''
        if self._use_tuples:
            if isinstance(spt, str):
                spt = tuple(spt.lower().split())
            
            t_lower = tuple([word.lower() for word in spt])
            if spt in self.base_map:
                base = self.base_map.get(spt)
            elif t_lower in self.base_map:
                base = self.base_map.get(t_lower)
            else:
                base = self.base_map.get(tuple([NLTK_STEMMER(word) for word in t_lower]))
        else:
            if isinstance(spt, (tuple, list)):
                spt = ' '.join(spt)
            
            if spt in self.base_map:
                base = self.base_map.get(spt)
            elif spt.lower() in self.base_map:
                base = self.base_map.get(spt.lower())
            else:
                base = self.base_map.get(NLTK_STEMMER(spt).lower())
            
        stem = self.stem_map.get(base) if base else self.stem_map.get(spt.lower())
        i_stem_count = self.stems.get(stem,0)
        d_confidence = min(1.0, math.log(i_stem_count + 1, self.max_stem) ** 0.5)
        return d_confidence
        
        
class PTypeEnhancements(BasePTypes):
    def __init__(self, spts, **kwargs):
        super().__init__(spts, **kwargs)
        ptype_modifiers = set()
        self.split_pts = [self.split_base(pt) for pt in self.pts]
        
        self.base_modifiers = defaultdict(Counter)  # modifiers for each base pt
        for t_modifier, t_base in self.split_pts:
            self.base_modifiers[t_base][t_modifier] += 1
            if len(t_modifier) > 1:
                for word in t_modifier:
                    self.base_modifiers[t_base][(word,)] += 1
    
        self.all_enhancements = set([split[0] for split in self.split_pts
                                     if split[0]])
        self.all_enhancements = set([enh for enh in self.all_enhancements
                                     if all(word not in c_NUMS for word in enh)])
        
        self.all_enhancement_words = set(word for enhancement in self.all_enhancements for word in enhancement)
        for word in c_EXCLUDE:
            if word in self.all_enhancement_words: self.all_enhancement_words.remove(word)
        for num in c_NUMS:
            if num in self.all_enhancement_words: self.all_enhancement_words.remove(num)
        
        
        self.enhancements = defaultdict(Counter)
        for enhancement, base in self.split_pts:
            if not base or not enhancement: continue
            self.enhancements[base][enhancement] += 1
    
    def get_base(self, spt):
        ''' Get just the base from the pt. '''
        if isinstance(spt, str):
            spt = tuple(word_tokenize(spt.lower(), stop_words=False, convert_ampersand=True))
        elif isinstance(spt, list):
            spt = tuple(spt)
        
        i_base_len = len(spt)
        while i_base_len > 1:
            if spt[-i_base_len:] in self.bases:
                break
            elif i_base_len == len(self.base_map.get(spt[-i_base_len:], [])):
                break
            elif i_base_len == len(self.stem_map.get(spt[-i_base_len:], [])):
                break
            i_base_len -= 1
        
        return spt[-i_base_len:]
    
    def split_base(self, spt):
        ''' Split the pt into enhancements and base. '''
        if isinstance(spt, str):
            spt = tuple(word_tokenize(spt.lower(), stop_words=False, convert_ampersand=True))
        elif isinstance(spt, list):
            spt = tuple(spt)
        
        i_base_len = len(spt)
        while i_base_len > 1:
            if spt[-i_base_len:] in self.bases:
                break
            elif i_base_len == len(self.base_map.get(spt[-i_base_len:], [])):
                break
            elif i_base_len == len(self.stem_map.get(spt[-i_base_len:], [])):
                break
            i_base_len -= 1
        
        return (spt[:-i_base_len], spt[-i_base_len:])
