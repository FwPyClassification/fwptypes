try:
    from mc_ptypes import *
except:
    from .mc_ptypes import *
try:
    from taxonomy import *
except:
    from .taxonomy import *
try:
    from base_ptypes import *
except:
    from .base_ptypes import *
try:
    from spt_selector import *
except:
    from .spt_selector import *
    
__version__ = "1.0.0"
