'''
Test cases for FwPTypes/mc_ptypes
'''
from __future__ import absolute_import
import pyximport
import math
import pandas as pd
import FwText, FwFreq
from FwPTypes import base_ptypes
pyximport.install()


class TestBasePTypes():
    def test_empty(self):
        o_bases = base_ptypes.BasePTypes([])
        assert isinstance(o_bases.ends, dict)
        assert isinstance(o_bases.bases, dict)
        assert not o_bases.ends
        assert not o_bases.bases

    def test_normal(self):
        o_bases = base_ptypes.BasePTypes([
            'cane',
            'walking cane',
            'servo for rc',
            'chocolate sundae with fudge',
            'servo',
            'chocolate sundae',
            'large chocolate sundae',
        ])
        assert isinstance(o_bases.ends, dict)
        assert isinstance(o_bases.bases, dict)
        assert o_bases.ends == {
            ('cane',): 2,
            ('chocolate', 'sundae'): 3,
            ('servo',): 2
        }
        assert o_bases.bases == {
            ('cane',): 2,
            ('chocolate', 'sundae'): 3,
            ('servo',): 2,
            ('sundae',): 3
        }
